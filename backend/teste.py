from flask import Flask, request
import json
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app)

@app.route("/users", methods=["POST", "GET"])
@cross_origin()
def teste():
    _request = dict(request.headers)
    _request = _request['Authorization']

    print(_request)


    print("bateu")
    return json.dumps([{
        'id': 234,
        'username': "string",
        'password': "string",
        'firstName': "string",
        'lastName': "string",
        'token': "string"
    },{
        'id': 2,
        'username': "string1",
        'password': "string2",
        'firstName': "string2",
        'lastName': "string2",
        'token': "string2"
    }])

@app.route("/users/authenticate", methods=['POST', 'GET'])
def users_authenticate():
    return json.dumps({
        'id': 234,
        'username': "string",
        'password': "string",
        'firstName': "string",
        'lastName': "string",
        'token': "string"
    })


app.run(debug=True, port=8085)